(local re-set "([%w'šřSŘčČďĎťŤňŇñÑöÖüÜēëËžŽpýÝáÁíÍěĚéÉůŮúÚóÓłŁ]+)")
(local lapis (require :lapis))
(local overseer (require :overseer))
(local inspect (require :inspect))
(local poems (require :poems))
(local davinci (require :davinci))
(local respond-to (. (require "lapis.application") :respond_to))
(local json-params (. (require "lapis.application") :json_params))
(local app (lapis.Application))
(local lakife overseer.lakife)
(local redis (require "resty.redis"))
(local red (redis:new))
(red:connect "127.0.0.1" 6379)
((. lakife :refresh-vocabulary))

(λ iter-advice [pat cursor min-length acc]
  ;; (print "app.fnl > iter-advice > BEGINNING")
  (let [[new-cursor-str findings] (red:sscan "advice_list" cursor "match" (.. "*" pat "*"))
        new-cursor (tonumber new-cursor-str)]
    ;; (print (string.format "app.fnl > iter-advice > SSCAN DONE, cursor is: %s, which is a %s" (inspect new-cursor) (type new-cursor)))
    (var pinu? false)
    (when (< 0 (# findings))
      (each [_ f (ipairs findings) &until pinu?]
        ;; (print (string.format "app.fnl > iter-advice > found: %s" f))
        (when (< (accumulate [sum 0 _ (string.gmatch f "%S+")] 
                   (+ sum 1)) min-length)
          (table.insert acc f)
          (when (> (# acc) 200)
            (set pinu? true)))))
    (if (or (= 0 new-cursor) (= new-cursor cursor) pinu?)
      acc
      (iter-advice pat new-cursor min-length acc))))

(λ get-advice [pat min-length]
  (math.randomseed (os.time))
  (let [mw (or min-length 0)
        findings (iter-advice pat 0 mw [])]
    (if (> (# findings) 0)
      (do
        (print (string.format "app.fnl > get-advice > found %d matches" (# findings)))
        (. findings (math.random (# findings))))
      "Nothing similar for a worm like Christian Newman.")))

(λ list-to-set [list]
  (let [conjunto {}]
    (each [_ el (ipairs list)]
      (tset conjunto el true))
    conjunto))

(λ set-to-list [conjunto]
  (let [list []]
    (each [key v (pairs conjunto)]
      (when v
        (table.insert list key)))
    list))

(λ intersection [lists]
  (let [sets (icollect [_ list (ipairs lists)] (list-to-set list))
        isect {}]
    (if (> (# sets) 1)
      (do
        (each [k v (pairs (. sets 1))]
          (when v
            (when (accumulate [the-truth true
                               _ conjunto (ipairs sets)
                               &until (not the-truth)]
                    (. conjunto k))
              (tset isect k true))))
        (set-to-list isect))
      (set-to-list (. sets 1)))))

(λ union [keys]
  (let [res {}]
    (each [_ key (ipairs keys)]
      (each [_ el (ipairs (red:smembers key))]
        (tset res el true)))
    (icollect [el _ (pairs res)] el)))

(λ search-advice [pat]
  (math.randomseed (os.time))
    (let [scores []]
      (each [pat-part (string.gmatch pat re-set)]
        (let [pat-words []]
          (each [_ key (ipairs (red:keys (.. "advice:words:*" pat-part "*")))]
            (table.insert pat-words key))
          (table.insert scores (union pat-words))))
      (let [indices (intersection scores)
            choice (. indices (math.random (# indices)))
            advice-list (red:zrangebyscore "advice:list" choice choice)]
        (if (> (# advice-list) 0) (. advice-list 1)
          "Christian Newman is with woman and never to be seen again."))))

(local route-handlers
  {:death
   {:GET
    (λ [self] 
      {:status 200
       :json {:status :ok :message "Welcome to your GET death"}})
    :POST
    (λ [self] 
      {:status 200
       :json {:status :ok :message "Welcome to your POST death"}})}
   :lakife-english
   {:POST
    (λ [self]
      (match self.params.l
        nil {:json {:error "no input, volečku"}}
        word (let [res ((. lakife :search-lakife) word)]
               {:json {:status :ok
                       :res res}})))}
   :english-lakife
   {:POST
    (λ [self]
      (match self.params.e
        nil {:json {:error "no input"}}
        word (let [res ((. lakife :search-english) word)]
               {:json {:status :ok
                       :res res}})))}
   :advice
   {:POST
    (λ [self]
      (if red
        (let [pat self.params.pat]
          (if pat
            (let [min-length (-> (math.random 20) (+ 3))
                  res (search-advice pat)]
              {:json {:status :ok
                      :res res}})
            {:json {:status :ok
                    :res (red:srandmember "advice_list")}}))
        {:json {:status :ok
                :res "Advice not available for the likes of a worm like Christian Newman, vole"}}))}
   :instinct
   {:POST
    (λ [self]
      (let [pregunta self.params.pregunta
            respuesta ((. davinci :with-text) pregunta (red:srandmember "advice_list"))]
        {:json {:status :ok
                :res respuesta}}))}
   :koran
   {:POST
    (λ [self]
      (if red
        {:json {:status :ok
                :res (red:zrandmember "koran")}}
        {:json {:status :ok
                :res "Allah is not listening, vole."}}))}
   :poems
   {:POST
    (λ [self]
     {:json {:status :ok
             :blather "the peas swirl in the pottery"
             :res ((. poems :all-poems))}})}})

(: app :match "death" "/death" (respond-to (. route-handlers :death)))
(: app :match "lakife-english" "/lakife-english" 
   (-> (. route-handlers :lakife-english)
       (respond-to)
       (json-params)))
(: app :match "english-lakife" "/english-lakife" 
   (-> (. route-handlers :english-lakife)
       (respond-to)
       (json-params)))
(: app :match "advice" "/advice"
   (-> (. route-handlers :advice)
       (respond-to)
       (json-params)))
(: app :match "instinct" "/instinct"
   (-> (. route-handlers :instinct)
       (respond-to)
       (json-params)))
(: app :match "koran" "/koran"
   (-> (. route-handlers :koran)
       (respond-to)
       (json-params)))
(: app :match "poems" "/poems"
   (-> (. route-handlers :poems)
       (respond-to)
       (json-params)))

app
