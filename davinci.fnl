(local inspect (require :inspect))
(local api-key "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")
(local api-endpoint "https://api.openai.com/v1/chat/completions")
(local system-messages ["You are a Nordic God who is just learned a little bit of Esperanto."
                        "You are a 18th century goat herder with extensive knowledge of quantum physics."
                        "You are a rodent specialist."
                        "You are a moody 90s grunge fan."
                        "You are an expert on 80s and 90s avant garde progressive rock."
                        "You are a cheerful 12 year old girl from Scotland."
                        "You are a recovering alcoholic who is trying to save his opera career."
                        "You are a plaintive know-it-all."
                        "You are reluctant to give out information."
                        "You are a conspiracy theorist who dabbles in goat herding."
                        "You own a pomagranate orchard and fluently speak English, Spanish, Czech and Esperanto."
                        "You love to talk about different kinds of tea and how they relate to your daily life."])
(local preludes ["Reply by pretending you are a dyslexic wallaby from Pluto."
                 "Reply knowing that you bow to Allah five times daily and she provides your meals and shelter."
                 "Answer as if a pomagranate were knocking you upside the head."
                 "Classify the following into between seven and twelve categories."
                 "Answer like you are a thirteenth century goat herder."
                 "Reply in the context of a detached, monolithic testicle of a mamba."
                 "As a post-world war three marmot, I scan the horizon for the newest analog bass synthesizers."
                 "Answer in both French and Czech"
                 "Reply in German"
                 "Answer in Czech"
                 "Translate into Slovak and then answer the question in Spanish"
                 "Reply in both Mongolian and Czech"
                 "Relate the following text to astrology"
                 "Relate the following text to numerology"
                 "Answer the question, but at the same time pretend you are distracted by the concept of frequency modulation synthesis."
                 "Reply to the question, but at the same time pretend you are a little distracted by the superior quality of your homemade goat cheese."
                 "Reply while eating a pomegranate"
                 "Answer as if you were a 18th century goat herder in Andalucía"
                 "Answer in both Spanish and Arabic and elaborate a bit in Esperanto"
                 "Reply in Spanish, but don't take the question too seriously"
                 "Reply in Czech, but inject a few ideas about political economics at the same time."
                 "Reply factually, but with a bit of humor about political economics at the same time."
                 "Answer in English, but don't take the question too seriously"
                 "Answer in English and French, and pretend you are a court jester in your reply."
                 "Make sure to inject a dollop of absurdity into the reply"
                 "Classify the following into as many related chemical compounds as possible"
                 "Answer partially in Latin."])
(local https (require "ssl.https"))
(local ltn12 (require :ltn12))
(local luna (require "lunajson"))
(local redis (require "resty.redis"))
(local red (redis:new))
;;(red:connect "127.0.0.1" 6379)
(local body {:model "gpt-3.5-turbo"
             :messages []
             :max_tokens 512
             :temperature 0.1})
(var current-conversation {:system "" :messages [] :word-count 0})

(λ count-words [s]
  (var c 0)
  (each [w in (string.gmatch s "%w+")]
    (set c (+ c 1)))
  c)

(λ join [arr] 
  (var s "") 
  (each [_ m in (ipairs arr)] 
    (set s (.. s " " m)))
  s)

(λ death-to-multi-lined-hovno [s]
  (let [arr []]
    (each [line (string.gmatch s "[^\n]+")]
      (table.insert arr line))
    (join arr)))

(λ append-to-bits-of-past-conversations [msg]
  (let [f (io.open "/home/polaris/infinite_bliss/bits-of-past-conversations" "a")]
    (f:write (string.format "USER: %s\n" (-> (. msg :user)
                                             (death-to-multi-lined-hovno))))
    (f:write (string.format "ASSISTANT: %s\n" (-> (. msg :assistant)
                                                  (death-to-multi-lined-hovno))))
    (f:close)))

(λ get-bit-of-past-conversations []
  (os.execute "touch /home/polaris/infinite_bliss/bits-of-past-conversations")
  (let [bits-file (io.popen "wc /home/polaris/infinite_bliss/bits-of-past-conversations")
        wc (bits-file:read)
        lines-count-str (string.match wc "(%d+)%s+%d+%s+%d+%s")
        lines-count (tonumber lines-count-str)]
    (if (> lines-count 0)
      (let [line-number (-> lines-count
                            (/ 2)
                            (math.floor)
                            (math.random))
            tail-amt (- lines-count (- (* line-number 2) 2))
            pipe (io.popen (string.format "cat /home/polaris/infinite_bliss/bits-of-past-conversations | tail -%d | head -2" tail-amt))
            message {}
            plaintive (λ []
                        (let [line (pipe:read)]
                          (if line line nil)))]
        (each [line in plaintive]
          (let [m (string.match line "^USER:%s(.+)$")]
            (when m
              (tset message :user m)))
          (let [m (string.match line "^ASSISTANT:%s(.+)$")]
            (when m
              (tset message :assistant m))))
        (pipe:close)
        (when (and (. message :user) (. message :assistant))
          (if (< (math.random) 0.2)
            message
            nil)))
      nil)))

(λ get-current-conversation []
  (os.execute "touch /tmp/current-conversation")
  (let [lines-fn (io.lines "/tmp/current-conversation")]
    (var message {})
    (each [line in lines-fn]
      (let [m (string.match line "^SYSTEM:%s(.+)$")]
        (when m
          (tset current-conversation :system m)))
      (let [m (string.match line "^USER:%s(.+)$")]
        (when m
          (tset message :user m)))
      (let [m (string.match line "^ASSISTANT:%s(.+)$")]
        (when m
          (tset message :assistant m)))
      (when (< (math.random) 0.1)
        (let [bit-of-past-conversation (get-bit-of-past-conversations)]
          (when bit-of-past-conversation
            (set message bit-of-past-conversation))))
      (when (and (. message :user) (. message :assistant))
        (table.insert (. current-conversation :messages) message)
        (tset current-conversation :word-count
              (+ (. current-conversation :word-count)
                 (count-words (. message :user))
                 (count-words (. message :assistant))))
        (set message {})))))

(λ save-current-conversation []
  (let [f (io.open "/tmp/current-conversation" "w")]
    (f:write (string.format "SYSTEM: %s\n" (. current-conversation :system)))
    (each [_ msg in (ipairs (. current-conversation :messages))]
      (when true ;; (< (math.random) 0.8)
        (f:write (string.format "USER: %s\n" (-> (. msg :user)
                                                 (death-to-multi-lined-hovno))))
        (f:write (string.format "ASSISTANT: %s\n" (-> (. msg :assistant)
                                                      (death-to-multi-lined-hovno))))))
    (f:close)))

(λ trim [s]
  (let [inner-peace (string.match s "^%s*(.-)%s*$")]
    (or inner-peace "")))

(λ has-hovno? [s]
  (-> (or (string.match s "an AI language model")
          (string.match s "treat others with kindness")
          (string.match s "provide more context")
          (string.match s "specific questions or concerns")
          (string.match s "speculation or unfounded")
          (string.match s "please feel free to ask"))
      (not)
      (not)))

(λ split-sentences [s]
  (let [s-array []]
    (each [whop in (string.gmatch s "[^%.!%?]+")]
      (table.insert s-array (trim whop)))
    s-array))

(λ get-prelude [p]
  (if (< (math.random) 0.4)
    p
    (. preludes (math.random (# preludes)))))

(λ make-prompt [msg p]
  (if (< (math.random) 1.0)
    (string.format "%s\n\n%s" (get-prelude p) msg)
    (string.format "%s\n\n%s" msg (get-prelude p))))

(λ set-system-message []
  (tset current-conversation :system (. system-messages (math.random (# system-messages)))))

(λ clean-current-conversation []
  (let [new-conversation {:messages [] :word-count 0}]
    (tset new-conversation :system (. current-conversation :system))
    (each [_ msg in (ipairs (. current-conversation :messages)) &until (> (. new-conversation :word-count) 2048)]
      (when (< (math.random) 0.333)
        (let [c (+ (count-words (. msg :user)) (count-words (. msg :assistant)))]
          (tset new-conversation :word-count (+ (. new-conversation :word-count) c))
          (table.insert (. new-conversation :messages) msg)))
      ;;(when (and (< (math.random) 0.1) (> (# bits-of-past-conversations) 1))
        ;;(let [msg (. bits-of-past-conversations (math.random (# bits-of-past-conversations)))]
              ;;c (+ (count-words (. msg :user)) (count-words (. msg :assistant)))]
          ;;(tset new-conversation :word-count (+ (. new-conversation :word-count) c))
          ;;(table.insert (. new-conversation :messages) msg)))
      )
    (set current-conversation new-conversation)
    (when (< (math.random) 0.2)
      (set-system-message))))

(λ make-messages [outgoing-message]
  (print (. current-conversation :word-count))
  (when (> (. current-conversation :word-count) 2048)
    (clean-current-conversation))
  (when (= (. current-conversation :system) "")
    (set-system-message))
  (let [message [{:role "system" :content (. current-conversation :system)}]]
    (each [_ msg in (ipairs (. current-conversation :messages))]
      (table.insert message {:role "user" :content (. msg :user)})
      (table.insert message {:role "assistant" :content (. msg :assistant)}))
    (table.insert message {:role "user" :content outgoing-message})
    message))

(λ with-text [t p]
  (get-current-conversation)
  (math.randomseed (os.time))
  (when (< (math.random) 0.05)
    (set-system-message))
  (let [prompt (make-prompt t p)
        temperature (-> (math.random) (/ 2) (+ 0.2))]
    (var resp {})
    (tset body :messages (make-messages prompt))
    (tset body :temperature temperature)
    (https.request {:url api-endpoint
                    :headers {:authorization (string.format "Bearer %s" api-key)
                              "content-type" "application/json"}
                    :method :POST
                    :source (ltn12.source.string (luna.encode body))
                    :sink (ltn12.sink.table resp)})
    (let [mimic (luna.decode (. resp 1))
          incoming-message (. mimic :choices 1 :message :content)
          split-message (split-sentences incoming-message)
          relevant-parts []]
      (each [_ s in (ipairs split-message)]
        (when (not (has-hovno? s))
          (table.insert relevant-parts s)))
      (when (> (# relevant-parts) 0)
        (let [message {:user t :assistant (join relevant-parts)}]
          (table.insert (. current-conversation :messages) message)
          (when (< (math.random) 0.6)
            (append-to-bits-of-past-conversations message))))
      (save-current-conversation)
      (tset mimic :prompt prompt)
      mimic)))

{: with-text}
