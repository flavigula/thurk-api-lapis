(local inspect (require :inspect))
(local poems-dir "/home/polaris/arch-my-hive/poems")
(local poems-re "%.txt$")

(λ process [filename]
  (let [path (.. poems-dir "/" filename)
        poem {:title ""
              :date ""
              :text []}
        lines-fn (io.lines path)]
    (each [line lines-fn]
      (if (-> (. poem :title)
              (string.len)
              (= 0))
        (tset poem :title line)
        (-> (. poem :date)
            (string.len)
            (= 0))
        (tset poem :date line)
        (string.match line "^%-%-")
        (print "got ---")
        (let [text (. poem :text)]
          (table.insert text line)
          (tset poem :text text))))
    poem))

(λ poem-files []
  (let [filenames []
        pipe (io.popen (string.format "ls %s" poems-dir))]
    (λ iter []
      (let [sylet (pipe:read)]
        (if (= sylet nil) filenames
          (string.match sylet poems-re)
          (do
            (table.insert filenames sylet)
            (iter))
          (iter))))
    (iter)))

(λ all-poems []
  (let [filenames (poem-files)
        poems []]
    (each [_ filename (ipairs filenames)]
      (table.insert poems (process filename)))
    poems))

{: poem-files : process : all-poems}
