local fennel = require("fennel")
fennel.path = fennel.path .. ";/home/polaris/rummaging_round/lua/?.fnl;/home/polaris/rummaging_round/lua/?/?.fnl;/?.fnl"

local redis = require "resty.redis"
print("advice_list.lua > REDIS REQUIRED")
local red = redis:new()
print("advice_list.lua > REDIS INITIALIZED")
red:set_timeouts(1000, 1000, 1000)
local ok, err = red:connect("127.0.0.1", 6379)
print("advice_list.lua > REDIS CONNECTED")
if ok then
  local advice_size = res:smembers("advice_list")
  print(string.format("advice_list.lua > advice_size: %s", advice_size))
  if not advice_size or advice_size == 0 then
    local into_jsons = fennel.dofile("../advice/into-jsons.fnl")
    local advice_list = into_jsons.abomination()
    if ok then
      for _, a in ipairs(advice_list) do
        red:sadd("advice_list", a)
      end
    end
  end
end
print("advice_list.lua > finished init")

if ok then
  return true
else
  return nil
end
