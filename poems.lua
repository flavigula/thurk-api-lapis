local fennel = require("fennel")
fennel.path = fennel.path .. ";/home/polaris/rummaging_round/lua/?.fnl;/home/polaris/rummaging_round/lua/?/?.fnl;/?.fnl"
local poems = fennel.dofile("./poems.fnl")
return poems
